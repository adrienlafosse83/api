---
marp: true
theme: uncover
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

# HyperText Transfer Protocol

---
<style scoped>
li {
  font-size: 35px;
}
</style>
# HTTP

- Application protocol for distributed hypermedia systems.
- Normalized in [RFC 9110](https://www.rfc-editor.org/rfc/rfc9110.html) by the [Internet Engineering Task Force (IETF)](https://www.ietf.org/about/introduction/)
- Foundation of the WWW communications
    - Request / response
    - Stateless
    - Media independent
- Current version: [HTTP/2](https://www.rfc-editor.org/rfc/rfc9113.html), [HTTP/1.1](https://www.rfc-editor.org/rfc/rfc9112.html) still used a lot
- [HTTP/3](https://www.rfc-editor.org/rfc/rfc9114.html) in proposal to make use of the new transport protocol [QUIC](https://www.rfc-editor.org/rfc/rfc9000.html)
<!-- 
Client/ Server
Stateless (no link between consecutives requests) but not sessionless (with use of cookies, using Headers)
HTTP2 is used for around 40% of websites
-->

---

# HTTP/2 

- Faster than its predecessors
- Only one TCP connection
- Option to server push to client
- Browser only support HTTP/2 over TLS

<!--- 
- HTTP/2 is more secure as it uses **binary protocol** instead of plaintext.
- Multiplexing: HTTP2 uses a single TCP connection to transmit requests and frames, thus eliminating the need for multiple connections.
- HTTP/2 **streaming** uses a prioritization tree for more efficient transmission.
- HTTP/2 reduced latency by using HPACK compression to shrink the size of headers
- HTTP/2 gives an option of **server push** to clients to further speed up the process.

ex: curl -v google.com -> HTTP/1.1 https://google.com -> HTTP/2

telnet google.com 80
Trying 2a00:1450:4007:819::200e...
Connected to google.com.
Escape character is '^]'.
GET / HTTP/1.1
Host: google.com
--->

---

# Requests

![bg right:50% 100%](assets/http_request.png)

- Request Line
- Request Headers
- Empty line
- Optional message body

---

# Responses

![bg right:50% 100%](assets/http_response.png)

- Status line
- Response Headers
- Empty line
- Optional message body

---

## URL - Unified Resource Locator

![url](assets/url.png)

URLs are percent encoded

<!---
Fragment not send to server, only for browser (for example anchors in page)
--->

---

<style scoped>
li {
  font-size: 35px;
}
</style>

# Methods (Verbs)

- **GET**: retrieve a resource
  - Safe: It must not modify resources
  - Idempotent: 1 call, same as multiple calls
  - Cachable
- **POST**: Add a resrouce
  - Not safe, not idempotent
  - Cachable if freshness information is included
- **PUT**: Create or Replace a resource
  - Idempotent
- **DELETE**: Deletes a resource
  - Idempotent

<!---
- Thode methods allow for Create, Read, Update, Delete operations
- An HTTP method is **safe** if it doesn't alter the state of the server
- An HTTP method is idempotent if the intended effect on the server of making a single request is the same as the effect of making several identical requests.
- All safe methods are also idempotent, but not all idempotent methods are safe
- safe/idempotent: Only semantic, no constraint in protocol
--->

---

# Other Methods 

- **PATCH**: Partial update
  - Not safe, not necessarly idempotent
- **HEAD**: GET withtout body
- **TACE**: Echo request back to the sender
- **OPTIONS**: List allowed Methods for an endpoint
- **CONNECT**: Connects to proxy

---
<style scoped>
li {
  font-size: 30px;
}
</style>

# [Headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers)

- Let the client and the server pass additional information

- [Request Headers](https://developer.mozilla.org/en-US/docs/Glossary/Request_header): only for request messages
  - Authorization, Accept-* , User-Agent, Host, Cookie, ...
- [Response Headers](https://developer.mozilla.org/en-US/docs/Glossary/Response_header): only for response messages
  - Agen Location, Server, Set-Cookie, ...
- [Representation Headers](https://developer.mozilla.org/en-US/docs/Glossary/Representation_header): representation of the resource
  -  Content-Type, Content-Encoding, Content-Language, Content-Location, ...
- [Payload Headers](https://developer.mozilla.org/en-US/docs/Glossary/Payload_header): Describes the payload information
  -  Content-Length, Content-Range, Trailer, Transfer-Encoding, ...

<!---
Interesting header we don't often manipulate ourselves: Host header positioned by User-Agent, containing target host.
Used for example by Kubernetes ingress to route traffic to correct host.
In HTTP/2, replaced by pseudo Header ":authority".
All pseudo headers begin with ":", they replace HTTP/1 request line and status line
--->

---
<style scoped>
li {
  font-size: 30px;
}
</style>

# HTTP/2's pseudo-headers

- Introducted in HTTP/2
- They replace the request & status lines
- Used for header compression
  - **:method** - The HTTP method of the request, such as GET or POST.
  - **:path** - The request path. Note that this includes the query string.
  - **:authority** - Roughly equivalent to the Host header in HTTP/1.
  - **:scheme** - The request scheme, typically http or https. Note that there is no equivalent in HTTP/1.
  - **:status** - The response status code.

---

# Request Parameters

- Query parameters: query strings as key/value paris in the URL
  - curl -X GET -H 'private-token: <redacted>' https://gitlab.com/api/v4/projects\?per_page=5&page=1
- Path parameters: query strings are part of the path:
  -  curl -X GET -H 'private-token: <redacted>' https://gitlab.com/api/v4/projects/<project_id>

---
<style scoped>
li {
  font-size: 30px;
}
</style>

# Request Parameters

- Some parameters can be sent, depending on headers, using:
  - "**Content-type: application/x-www-form-urlencoded**": 
  The content body contains key/value pairs (*field1=value1&field2=value2&field3=value3*).
  - "**Content-Type: application/json**": 
  JSON formated content body (*'{"field1": "value1", "field2": "value2", "field3": "value3"}'*).
  - "**Content-type: "multipart/form-data**" for binary data:
  Special format using several parts separated with a particular string boundary (content-disposition header), each part having its own content-type header

<!---
Usually for POST/PUT/PATCH
--->
---
<style scoped>
li {
  font-size: 30px;
}
</style>

# Status codes

- **1xx: Informational responses**
- **2xx: Successful responses**
  - **200 OK**: basic success. (GET, PUT, PATCH)
  - **201 Created**: Resource was created (PUT/POST)
  - **202 Accepted**: Request accepted for processing. typically for Asynchronous proccessing.
  - **204 No Content**: Request succeeded but nothing to show (DELETE)
- **3xx: Redirection messages**
  - **301 Moved Permanently**: requested resource has been definitively moved to the URL given by the Location headers
  - **304 Not Modified**: no need to retransmit the requested resources (cached)

---
<style scoped>
li {
  font-size: 30px;
}
</style>
# Status codes

- 4xx: Client Error responses
  - **400 Bad Request**: Reques cannot be processed
  - **401 Unauthorized**: Not *Authenticated*
  - **403 Forbidden**: Not *Authorized*
  - **404 Not Found**: Resource does not exist
  - **405 Method Not Allowed**: Method not supported, or user does not have permission
- **5xx: Server Error responses**
  - **500 Internal Server Error**: The request seem right, but an error occured on the server. The client cannot do anything about it.
  - **503 Service Unavailable**: The server is not ready to handle the request

---
<style scoped>
li {
  font-size: 30px;
}
</style>
# Security

- Authorization Header
  - Allows different kind of authentication : basic, digest, oauth
  - "Authorization: {Type} {Data}"
  - Ex: "Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="
    - RFC2045-MIME variant of Base64 encoding of “login:password”
- Encryption with HTTPS = HTTP over SSL/TLS
  - Assymetric key derived into a short-term session key (symetric encryption)
  - Used to encrypt the whole HTTP data flow

<!---
We use symetric encryption during the session be cause of lower overhead
Asymetric encryption to exchange session key for security
--->

--- 

# Practical Work

---

- Download [Postman](https://www.postman.com/downloads/) & Create an account
- Take time to play and be familiar with Postman features
  - We’ll use it all along the training.
  - See how to create collections, to save them, …
  - You will commit your backup to a git repository so that i can collect them.

---

# [Gitlab](https://gitlab.com/)

- Open source code repository and collaborative software development platform.
- Powerfull CI/CD capabilities

- Go and create an account
- Play with it: Create a new project, create a Branch, a Merge Request

---
<style scoped>
li {
  font-size: 30px;
}
</style>

# Assignment

- Use Postman to make requests to Gitlab's REST API
- Documentation: https://docs.gitlab.com/ee/api/
- Using the API:
  - Create a [fork](https://docs.gitlab.com/ee/api/projects.html#fork-project) of the project https://gitlab.com/training788/api, name it with your surname.
  - List your projects
  - Create a new branch named "1-assignment"
  - Create a New Merge request using the previsouly created branch as the source branch

---
<style scoped>
li {
  font-size: 25px;
}
</style>

- Export pour Postman Collection
- Install a Git client if you don't already have one (for example Git bash for Windows)
- Configure your Git client:
  - `git config --global user.name `"FIRST_NAME LAST_NAME"
  - `git config --global user.email "MY_NAME@example.com"`
- Clone your project
```git clone https://gitlab.com/training788/api.git```
- Commit and push it in the previously created branch
  - Move in the project directory (for example `mv api`)
  - move the export file in the directory (You can use the `mv path/to/file.json .` command or the file explorer)
  - git checkout 1-assignment
  - git add .
  - git commit -m "My commit message"
  - git push
- Merge your Merge Request in Gitlab's interface
- Send me the URL to your project